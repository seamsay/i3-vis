# i3-vis

Visualise the i3 container tree and current key bindings.

## Installation

### Nix

If you use the Nix Package Manager you can install using the derivation on the master branch. If you don't know how to do that (or you were going to use `nix-env` anyway) you can use the following command:

```sh
nix-env -f https://gitlab.com/seamsay/i3-vis/-/archive/master/i3-vis-master.tar.gz --arg fetchFromGitLab "(import <nixpkgs> {}).fetchFromGitLab" --arg gtk3 "(import <nixpkgs> {}).gtk3" --arg rustPlatform "(import <nixpkgs> {}).rustPlatform" -i
```

### Cargo

Currently the only supported package manager is Nix, so most people will need to build from source. The easiest way of doing this is to install [Rust and Cargo](https://www.rust-lang.org/tools/install) and use this command:

```sh
cargo install --git https://gitlab.com/seamsay/i3-vis --branch release --root "$HOME/.local"
```

## Suggested Usage

Place this somewhere in your i3/sway configuration file:

```
for_window [title="^i3-vis:"] sticky enable
for_window [title="^i3-vis:"] move scratchpad

bindsym $mod+b [title="^i3-vis:keybindings$"] scratchpad show, focus mode_toggle
bindsym $mod+t [title="^i3-vis:tree$"] scratchpad show, focus mode_toggle

exec --no-startup-id i3-vis
```

This will open both windows (you can suppress the opening of either window, see `i3-vis --help`) when i3 starts, send them to the scratchpad, and set two keybindings to toggle their visibility.

## Bugs And Feature Requests

Feel free to open issues on the [GitLab repo](https://gitlab.com/seamsay/i3-vis) with any bugs you find or features that you want, I will _try_ to fix bugs relatively quickly but I can't promise that I will ever work on feature requests (please do open them though, because I might still get round to them at some point).

### Pull Requests

I'm happy to consider pull requests but please comment on the related issue (or open a new one if one doesn't exist) to discuss any work first. Also when writing the code please use [rustfmt](https://github.com/rust-lang/rustfmt) on your code, try to keep [clippy](https://github.com/rust-lang/rust-clippy) complaints to a minimum, and follow [conventional commit guidelines](https://www.conventionalcommits.org/en/v1.0.0-beta.2/) as closely as possible.
