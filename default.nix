{ fetchFromGitLab, gtk3, rustPlatform }:
rustPlatform.buildRustPackage rec {
  name = "i3-vis-${version}";
  version = "0.1.0";

  src = fetchFromGitLab {
    owner = "seamsay";
    repo = "i3-vis";
    rev = "v${version}";
    sha256 = "1v51af5sacwvp3qb0z4y3qx8nhnm945jh0mhhq8vv581wy56f9qn";
  };

  cargoSha256 = "0njidkwim3zdswl28fcv14iyg9fxq83wq15i7ciax08xy4vpw0f5";

  buildInputs = [ gtk3 ];
}
