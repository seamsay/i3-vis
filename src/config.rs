// TODO:
// 	- Write a proper parser, not just some ad-hoc line-based thing.
// 	- Handle line-continuations.
// 	- Handle `mode` blocks that aren't necessarily declared on one line.
// 	- Handle `bindcode` and mouse-bindings.

// TODO: Once const fn is here initialise all regexes globally.

use std::collections::HashMap;
use std::io::{BufRead, Read};
use std::path::Path;

const NO_GROUP: &str = "this group can't be absent from a matching regex";

/// Load the config file plus any files that it includes.
fn load_config<P: AsRef<Path>>(config: P) -> Result<String, String> {
	read_config(std::fs::File::open(&config).map_err(crate::format_error)?)
}

fn read_config<R: Read>(config: R) -> Result<String, String> {
	let lines = std::io::BufReader::new(config)
		.lines()
		.map(|el| el.map_err(crate::format_error))
		.collect::<Result<Vec<_>, _>>()?;

	let mut buffer = String::default();
	let include_rgx = regex::Regex::new(r"^\s*include\s+").map_err(crate::format_error)?;

	for line in lines {
		if let Some(m) = include_rgx.find(&line) {
			for path in glob::glob(&line[m.end()..]).map_err(crate::format_error)? {
				buffer.push_str(&load_config(path.map_err(crate::format_error)?)?)
			}
		} else {
			buffer.push_str(&line);
			buffer.push('\n');
		}
	}

	Ok(buffer)
}

fn variables(config: &str) -> Result<HashMap<String, String>, String> {
	let mut map = HashMap::default();
	let set_rgx = regex::Regex::new(r"^\s*set\s+\$(\S+)\s+(.+)").map_err(crate::format_error)?;

	for line in config.lines() {
		if let Some(m) = set_rgx.captures(&line) {
			map.insert(
				m.get(1).expect(NO_GROUP).as_str().into(),
				m.get(2).expect(NO_GROUP).as_str().into(),
			);
		}
	}

	Ok(map)
}

fn replace_variables(config: &str, variables: HashMap<String, String>) -> Result<String, String> {
	// As a rough guess, assume variables will increase size by 10%.
	let mut new = String::with_capacity(config.len() + config.len() / 10);

	let set_rgx = regex::Regex::new(r"^\s*set\s+").map_err(crate::format_error)?;
	let var_rgx = regex::Regex::new(r"\$(\w+)").map_err(crate::format_error)?;

	for line in config.lines() {
		if !set_rgx.is_match(&line) {
			new.push_str(&var_rgx.replace_all(&line, |c: &regex::Captures| {
				variables
					.get(c.get(1).expect(NO_GROUP).as_str())
					.expect("TODO: Can we return a result instead?")
			}));
			new.push('\n');
		}
	}

	Ok(new)
}

#[derive(Clone, Debug)]
pub struct Binding {
	pub key: String,
	pub command: String,
}

pub type Bindings = HashMap<String, Vec<Binding>>;

fn parse_bindings(config: &str) -> Result<Bindings, String> {
	let mut modes = Bindings::default();
	let mut mode = "default";

	let mode_rgx = regex::Regex::new(r#"^\s*mode(?:\s+--pango_markup)?\s+"(.+)"\s+\{"#)
		.map_err(crate::format_error)?;
	let bindsym_rgx = regex::Regex::new(r"^\s*bindsym(?:\s+--release)?\s+(\S+)\s+(.+)$")
		.map_err(crate::format_error)?;
	let end_rgx = regex::Regex::new(r"^\s*\}").map_err(crate::format_error)?;

	for line in config.lines() {
		if let Some(m) = mode_rgx.captures(&line) {
			mode = m.get(1).expect(NO_GROUP).as_str();
		} else if let Some(m) = bindsym_rgx.captures(&line) {
			let key = m.get(1).expect(NO_GROUP).as_str().into();
			let command = m.get(2).expect(NO_GROUP).as_str().into();
			let binding = Binding { key, command };

			modes
				.entry(mode.into())
				.or_insert_with(Vec::new)
				.push(binding);
		} else if end_rgx.is_match(&line) {
			mode = "default";
		}
	}

	Ok(modes)
}

pub fn load_bindings<P: AsRef<Path>>(path: P) -> Result<Bindings, String> {
	read_bindings(std::fs::File::open(&path).map_err(crate::format_error)?)
}

pub fn read_bindings<R: Read>(config: R) -> Result<Bindings, String> {
	let config = read_config(config)?;
	let config = replace_variables(&config, variables(&config)?)?;
	parse_bindings(&config)
}

// TODO: Test this module.
