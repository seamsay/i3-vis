// TODO: We can probably get rid of a lot of the owned things in this module.
// TODO: Do proper error handling, not just strings.

use i3ipc::reply::{Node, NodeLayout, NodeType};
use petgraph::graph;
use std::convert::TryFrom;

// TODO: pub const ROOT: graph::NodeIndex = graph::NodeIndex::new(0);
pub fn root() -> graph::NodeIndex {
	graph::NodeIndex::new(0)
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum ContainerType {
	/// Storage for other containers (i.e. a node that doesn't contain a window).
	Storage,
	/// A container that only contains an X11 window.
	Window,
	Workspace,
}

impl<'a> From<&'a Node> for ContainerType {
	fn from(node: &'a Node) -> Self {
		use self::ContainerType::*;
		match &node.nodetype {
			NodeType::Workspace => Workspace,
			NodeType::Con | NodeType::FloatingCon => match &node.window {
				Some(_) => Window,
				None => Storage,
			},
			ty => unimplemented!("container with type {:?}", ty),
		}
	}
}

#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum ContainerLayout {
	Horizontal,
	Stacked,
	Tabbed,
	Vertical,
}

impl<'a> From<&'a Node> for ContainerLayout {
	fn from(node: &'a Node) -> Self {
		use self::ContainerLayout::*;
		match &node.layout {
			NodeLayout::SplitH => Horizontal,
			NodeLayout::SplitV => Vertical,
			NodeLayout::Stacked => Stacked,
			NodeLayout::Tabbed => Tabbed,
			ty => unimplemented!("container with layout {:?}", ty),
		}
	}
}

/// Represents each container within a workspace.
#[derive(Debug, Eq, PartialEq)]
pub struct Container {
	pub id: i64,
	pub name: String,
	pub ty: ContainerType,
	pub layout: ContainerLayout,
	pub focused: bool,
}

impl<'a> From<&'a Node> for Container {
	fn from(node: &'a Node) -> Self {
		Container {
			id: node.id,
			name: node.name.clone().unwrap_or_else(|| "".into()),
			ty: node.into(),
			layout: node.into(),
			focused: node.focused,
		}
	}
}

/// Represents each workspace that exists on an output.
#[derive(Debug)]
pub struct Workspace {
	pub containers: graph::Graph<Container, ()>,
	pub focused: bool,
	pub name: String,
}

impl<'a> reingold_tilford::NodeInfo<graph::NodeIndex> for &'a Workspace {
	type Key = graph::NodeIndex;

	fn key(&self, node: graph::NodeIndex) -> Self::Key {
		node
	}

	fn children(&self, node: graph::NodeIndex) -> reingold_tilford::SmallVec<graph::NodeIndex> {
		self.containers.neighbors(node).collect()
	}

	fn dimensions(&self, _node: graph::NodeIndex) -> reingold_tilford::Dimensions {
		reingold_tilford::Dimensions {
			top: 0.03,
			right: 0.1,
			bottom: 0.03,
			left: 0.1,
		}
	}

	fn border(&self, _node: graph::NodeIndex) -> reingold_tilford::Dimensions {
		reingold_tilford::Dimensions::all(0.02)
	}
}

impl PartialEq for Workspace {
	fn eq(&self, other: &Self) -> bool {
		self.name == other.name
			&& self.focused == other.focused
			&& petgraph::algo::is_isomorphic_matching(
				&self.containers,
				&other.containers,
				|a, b| a == b,
				|a, b| a == b,
			)
	}
}

impl Eq for Workspace {}

impl<'a> TryFrom<&'a Node> for Workspace {
	type Error = String;

	fn try_from(tree: &'a Node) -> Result<Self, Self::Error> {
		match tree.nodetype {
			NodeType::Workspace => {}
			_ => return Err("root node is not a workspace".into()),
		}

		let name = tree
			.name
			.clone()
			.expect("workspace containers must have a name");

		let mut containers = graph::Graph::default();
		let mut focused = tree.focused;

		let parent = containers.add_node(tree.into());

		let mut stack = Vec::default();

		for node in &tree.nodes {
			stack.push((parent, node));
		}

		while let Some((parent, node)) = stack.pop() {
			focused = focused || node.focused;

			let child = containers.add_node(node.into());
			containers.add_edge(parent, child, ());

			for node in &node.nodes {
				stack.push((child, node));
			}
		}

		Ok(Workspace {
			containers,
			focused,
			name,
		})
	}
}

/// Represents each output (i.e. monitor) that the window manager knows about.
#[derive(Debug, Eq, PartialEq)]
pub struct Output {
	pub focused: bool,
	pub name: String,
	pub workspaces: Vec<Workspace>,
}

impl<'a> TryFrom<&'a Node> for Output {
	type Error = String;

	fn try_from(tree: &'a Node) -> Result<Self, Self::Error> {
		fn find_name<'n>(tree: &'n Node, name: &str) -> Result<&'n Node, String> {
			tree.nodes
				.iter()
				.find(|node| node.name == Some(name.to_string()))
				.ok_or_else(|| format!("unable to find child named `{}`", name))
		}

		match tree.nodetype {
			NodeType::Output => {}
			_ => return Err("root node is not an output".into()),
		}

		let workspaces = find_name(&tree, "content")?
			.nodes
			.iter()
			.map(Workspace::try_from)
			.collect::<Result<Vec<_>, _>>()?;

		Ok(Output {
			focused: workspaces.iter().any(|ws| ws.focused),
			name: tree
				.name
				.clone()
				.expect("output containers must have a name"),
			workspaces,
		})
	}
}

/// Represents the window manager itself.
#[derive(Debug, Eq, PartialEq)]
pub struct Manager(Vec<Output>);

impl std::ops::Deref for Manager {
	type Target = Vec<Output>;

	fn deref(&self) -> &Self::Target {
		&self.0
	}
}

impl std::ops::DerefMut for Manager {
	fn deref_mut(&mut self) -> &mut Self::Target {
		&mut self.0
	}
}

impl IntoIterator for Manager {
	type Item = Output;
	type IntoIter = std::vec::IntoIter<Self::Item>;

	fn into_iter(self) -> Self::IntoIter {
		self.0.into_iter()
	}
}

impl TryFrom<Node> for Manager {
	type Error = String;

	fn try_from(tree: Node) -> Result<Self, Self::Error> {
		Self::try_from(&tree)
	}
}

impl<'a> TryFrom<&'a Node> for Manager {
	type Error = String;

	fn try_from(tree: &'a Node) -> Result<Self, Self::Error> {
		Ok(Manager(
			tree.nodes
				.iter()
				.filter(|node| node.name != Some("__i3".into()))
				.map(Output::try_from)
				.collect::<Result<Vec<_>, _>>()?,
		))
	}
}

#[cfg(test)]
mod test {
	// TODO: Save some actual i3 trees for testing.

	use super::*;
	use ::i3ipc::reply::NodeBorder;

	struct NodeBuilder(Node);

	impl NodeBuilder {
		fn new() -> Self {
			NodeBuilder(Node {
				focus: vec![],
				nodes: vec![],
				floating_nodes: vec![],
				id: 0,
				name: None,
				nodetype: NodeType::Unknown,
				border: NodeBorder::Unknown,
				current_border_width: 0,
				layout: NodeLayout::Unknown,
				percent: None,
				rect: (0, 0, 0, 0),
				window_rect: (0, 0, 0, 0),
				deco_rect: (0, 0, 0, 0),
				geometry: (0, 0, 0, 0),
				window: None,
				urgent: false,
				focused: false,
			})
		}

		fn done(self) -> Node {
			self.0
		}

		fn id(self, id: i64) -> Self {
			NodeBuilder(Node { id, ..self.0 })
		}

		fn name(self, name: &str) -> Self {
			NodeBuilder(Node {
				name: Some(name.into()),
				..self.0
			})
		}

		fn nodetype(self, nodetype: NodeType) -> Self {
			NodeBuilder(Node { nodetype, ..self.0 })
		}

		fn layout(self, layout: NodeLayout) -> Self {
			NodeBuilder(Node { layout, ..self.0 })
		}

		fn window(self, window: i32) -> Self {
			NodeBuilder(Node {
				window: Some(window),
				..self.0
			})
		}
	}

	fn workspace_tree() -> (Node, Workspace) {
		let mut con = NodeBuilder::new()
			.id(1)
			.nodetype(NodeType::Con)
			.layout(NodeLayout::SplitH)
			.done();
		let con_child1 = NodeBuilder::new()
			.id(3)
			.name("App 2")
			.nodetype(NodeType::Con)
			.layout(NodeLayout::SplitH)
			.window(1)
			.done();
		let con_child2 = NodeBuilder::new()
			.id(4)
			.name("App 3")
			.nodetype(NodeType::Con)
			.layout(NodeLayout::SplitH)
			.window(2)
			.done();

		con.nodes = vec![con_child1, con_child2];

		let mut root = NodeBuilder::new()
			.id(0)
			.name("Workspace")
			.nodetype(NodeType::Workspace)
			.layout(NodeLayout::SplitH)
			.done();
		let root_child = NodeBuilder::new()
			.id(2)
			.name("App 1")
			.nodetype(NodeType::Con)
			.layout(NodeLayout::SplitH)
			.window(0)
			.done();

		root.nodes = vec![root_child, con];
		let node = root;

		let mut graph = graph::Graph::default();
		let root = graph.add_node(Container {
			id: 0,
			name: "Workspace".into(),
			ty: ContainerType::Workspace,
			layout: ContainerLayout::Horizontal,
			focused: false,
		});
		let con = graph.add_node(Container {
			id: 1,
			name: "".into(),
			ty: ContainerType::Storage,
			layout: ContainerLayout::Horizontal,
			focused: false,
		});
		let root_child = graph.add_node(Container {
			id: 2,
			name: "App 1".into(),
			ty: ContainerType::Window,
			layout: ContainerLayout::Horizontal,
			focused: false,
		});
		let con_child1 = graph.add_node(Container {
			id: 3,
			name: "App 2".into(),
			ty: ContainerType::Window,
			layout: ContainerLayout::Horizontal,
			focused: false,
		});
		let con_child2 = graph.add_node(Container {
			id: 4,
			name: "App 3".into(),
			ty: ContainerType::Window,
			layout: ContainerLayout::Horizontal,
			focused: false,
		});
		graph.add_edge(root, con, ());
		graph.add_edge(root, root_child, ());
		graph.add_edge(con, con_child1, ());
		graph.add_edge(con, con_child2, ());
		let workspace = Workspace {
			containers: graph,
			name: "Workspace".into(),
			focused: false,
		};

		(node, workspace)
	}

	fn output_tree() -> (Node, Output) {
		let (w_tree, w_expected) = workspace_tree();

		let mut output = NodeBuilder::new()
			.id(5)
			.name("eDP1")
			.nodetype(NodeType::Output)
			.layout(NodeLayout::SplitH)
			.done();
		let mut content = NodeBuilder::new()
			.id(6)
			.name("content")
			.nodetype(NodeType::Con)
			.layout(NodeLayout::SplitH)
			.done();
		content.nodes = vec![w_tree];
		output.nodes = vec![content];
		let node = output;

		let output = Output {
			focused: false,
			name: "eDP1".into(),
			workspaces: vec![w_expected],
		};

		(node, output)
	}

	mod container_type {
		use super::*;

		#[test]
		fn infer_storage_con() {
			let node = NodeBuilder::new().nodetype(NodeType::Con).done();

			assert_eq!(ContainerType::from(&node), ContainerType::Storage);
		}

		#[test]
		fn infer_storage_float_con() {
			let node = NodeBuilder::new().nodetype(NodeType::FloatingCon).done();

			assert_eq!(ContainerType::from(&node), ContainerType::Storage);
		}

		#[test]
		fn infer_window_con() {
			let node = NodeBuilder::new().nodetype(NodeType::Con).window(0).done();

			assert_eq!(ContainerType::from(&node), ContainerType::Window);
		}

		#[test]
		fn infer_window_floating_con() {
			let node = NodeBuilder::new()
				.nodetype(NodeType::FloatingCon)
				.window(0)
				.done();

			assert_eq!(ContainerType::from(&node), ContainerType::Window);
		}

		#[test]
		fn infer_workspace() {
			let node = NodeBuilder::new().nodetype(NodeType::Workspace).done();

			assert_eq!(ContainerType::from(&node), ContainerType::Workspace);
		}

		#[test]
		#[should_panic(expected = "container with type Unknown")]
		fn panic_on_other() {
			let node = NodeBuilder::new().nodetype(NodeType::Unknown).done();

			ContainerType::from(&node);
		}
	}

	mod container_layout {
		use super::*;

		#[test]
		fn infer_horizontal() {
			let node = NodeBuilder::new().layout(NodeLayout::SplitH).done();

			assert_eq!(ContainerLayout::from(&node), ContainerLayout::Horizontal);
		}

		#[test]
		fn infer_vertical() {
			let node = NodeBuilder::new().layout(NodeLayout::SplitV).done();

			assert_eq!(ContainerLayout::from(&node), ContainerLayout::Vertical);
		}

		#[test]
		fn infer_stacked() {
			let node = NodeBuilder::new().layout(NodeLayout::Stacked).done();

			assert_eq!(ContainerLayout::from(&node), ContainerLayout::Stacked);
		}

		#[test]
		fn infer_tabbed() {
			let node = NodeBuilder::new().layout(NodeLayout::Tabbed).done();

			assert_eq!(ContainerLayout::from(&node), ContainerLayout::Tabbed);
		}
	}

	mod container {
		use super::*;

		#[test]
		fn infer() {
			let node = NodeBuilder::new()
				.name("foo")
				.nodetype(NodeType::Con)
				.layout(NodeLayout::SplitH)
				.window(0)
				.done();

			let expected = Container {
				id: 0,
				name: "foo".into(),
				ty: ContainerType::Window,
				layout: ContainerLayout::Horizontal,
				focused: false,
			};

			assert_eq!(Container::from(&node), expected);
		}
	}

	mod workspace {
		use super::*;

		#[test]
		fn infer() {
			let (tree, expected) = workspace_tree();

			assert_eq!(Workspace::try_from(&tree).unwrap(), expected);
		}

		#[test]
		#[should_panic(expected = "root node is not a workspace")]
		fn panic_on_non_workspace() {
			let (mut tree, _) = workspace_tree();
			tree.nodetype = NodeType::Con;
			Workspace::try_from(&tree).unwrap();
		}
	}

	mod output {
		use super::*;

		#[test]
		fn infer() {
			let (tree, expected) = output_tree();

			assert_eq!(Output::try_from(&tree).unwrap(), expected);
		}

		#[test]
		#[should_panic(expected = "unable to find child named `content`")]
		fn panic_on_no_content() {
			let (mut tree, _) = output_tree();
			let content = tree.nodes.pop().unwrap();
			tree.nodes = content.nodes;
			Output::try_from(&tree).unwrap();
		}

		#[test]
		#[should_panic(expected = "root node is not an output")]
		fn panic_on_non_outspace() {
			let (mut tree, _) = output_tree();
			tree.nodetype = NodeType::Workspace;
			Output::try_from(&tree).unwrap();
		}
	}

	mod manager {
		use super::*;

		#[test]
		fn infer() {
			let (tree, expected) = output_tree();

			let mut root = NodeBuilder::new().id(100).nodetype(NodeType::Root).done();
			root.nodes = vec![tree];

			assert_eq!(Manager::try_from(&root).unwrap(), Manager(vec![expected]));
		}
	}
}
