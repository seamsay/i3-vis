// TODO:
//  - Stop using `String`s for all the errors!
//  - Add in some logging.
mod config;
mod tree;

use clap::{crate_authors, crate_description, crate_name, crate_version};

use gio::prelude::*;
use gtk::prelude::*;
use reingold_tilford::NodeInfo;
use std::convert::TryFrom;
use std::default::Default;

// HINT: The binary uses information from these files so we include them here to ensure they stay in sync.
#[cfg(debug_assertions)]
const _MANIFEST: &str = include_str!("../Cargo.toml");

// TODO: This will eventually become unecessary (I promise).
fn format_error<T: std::error::Error>(e: T) -> String {
	format!("{:?}", e)
}

// TODO: Only show one mode at a time (maybe some kind of filter?).
// TODO: Should this be based on ListBox instead?
fn keybindings(application: &gtk::Application, config: &config::Bindings) {
	let window = gtk::ApplicationWindow::new(application);

	window.set_title("i3-vis:keybindings");
	window.set_position(gtk::WindowPosition::Center);
	window.set_default_size(400, 400);

	let scroll = gtk::ScrolledWindow::new(None::<&gtk::Adjustment>, None::<&gtk::Adjustment>);
	scroll.set_shadow_type(gtk::ShadowType::EtchedIn);
	scroll.set_policy(gtk::PolicyType::Never, gtk::PolicyType::Automatic);
	window.add(&scroll);

	let store = {
		let column_types = [
			gtk::Type::String,
			gtk::Type::String,
			gtk::Type::String,
			gtk::Type::String,
		];
		let column_indices = [0, 1, 2, 3];

		fn sort_bindings(bindings: &config::Bindings) -> Vec<(&str, &[config::Binding])> {
			let mut items = bindings
				.iter()
				.map(|(k, v)| (k.as_ref(), v.as_ref()))
				.collect::<Vec<(&str, &[config::Binding])>>();

			items.sort_by(|(lk, _), (rk, _)| {
				if lk == &"default" {
					std::cmp::Ordering::Less
				} else if rk == &"default" {
					std::cmp::Ordering::Greater
				} else {
					lk.cmp(rk)
				}
			});

			items
		}

		let store = gtk::ListStore::new(&column_types);

		for (mode, bindings) in sort_bindings(config) {
			for config::Binding { key, command } in bindings {
				let help = String::new();
				let values: [&dyn ToValue; 4] = [&mode, &key, &command, &help];
				store.set(&store.append(), &column_indices, &values);
			}
		}

		store
	};

	let tree = gtk::TreeView::new_with_model(&store);
	scroll.add(&tree);

	for (i, title) in ["Mode", "Key", "Command", "Help"].iter().enumerate() {
		let i = i as i32;

		let renderer = gtk::CellRendererText::new();

		let column = gtk::TreeViewColumn::new();
		column.pack_start(&renderer, true);
		column.set_title(title);
		column.add_attribute(&renderer, "text", i);

		tree.append_column(&column);
	}

	window.show_all();
}

// TODO: Base this around Layout instead if DrawingArea (if that's possible, otherwise just do it better...).
fn tree(application: &gtk::Application) {
	let window = gtk::ApplicationWindow::new(application);

	window.set_title("i3-vis:tree");
	window.set_position(gtk::WindowPosition::Center);

	let area = gtk::DrawingArea::new();
	window.add(&area);

	let tree = std::rc::Rc::new(std::cell::RefCell::new(
		tree::Manager::try_from(i3ipc::I3Connection::connect().unwrap().get_tree().unwrap())
			.unwrap(),
	));

	let (tx, rx) = glib::MainContext::channel(glib::PRIORITY_DEFAULT);
	std::thread::spawn(move || {
		use i3ipc::Subscription;

		let mut connection = i3ipc::I3Connection::connect().unwrap();
		let mut listener = i3ipc::I3EventListener::connect().unwrap();
		listener
			.subscribe(&[
				Subscription::Output,
				Subscription::Window,
				Subscription::Workspace,
			])
			.unwrap();

		for event in listener.listen() {
			use i3ipc::event::Event::*;

			match event.unwrap() {
				OutputEvent(..) | WindowEvent(..) | WorkspaceEvent(..) => {
					connection
						.get_tree()
						.map_err(format_error)
						.and_then(tree::Manager::try_from)
						.and_then(|tree| tx.send(tree).map_err(format_error))
						.unwrap();
				}
				_ => unreachable!("Can't receive events for things we haven't subscribed to."),
			}
		}
	});

	let connect_draw_tree = tree.clone();
	area.connect_draw(move |area, cairo| {
		let draw_node = |x, y, width, height, container: &tree::Container| {
			let line_height = height / 3.0;
			let height_factor = 0.9;
			let text_height = height_factor * line_height;

			cairo.set_source_rgb(0.25, 0.25, 0.25);
			cairo.select_font_face("Sans", cairo::FontSlant::Normal, cairo::FontWeight::Normal);
			cairo.set_font_size(text_height);

			cairo.move_to(x, y + text_height);
			cairo.show_text(&format!("{:?}: {:?}", container.ty, container.layout));
			cairo.move_to(x, y + line_height + text_height);
			cairo.show_text(&format!("{}", container.id));
			cairo.move_to(x, y + 2.0 * line_height + text_height);
			cairo.show_text(&container.name);

			if container.focused {
				cairo.set_source_rgb(1.0, 0.0, 0.0);
			}

			cairo.set_line_width(0.001);
			cairo.rectangle(x, y, width, height);
			cairo.stroke();
		};

		let connect_draw_tree = connect_draw_tree.borrow();
		let workspace = &connect_draw_tree
			.iter()
			.find(|output| output.focused)
			.and_then(|output| output.workspaces.iter().find(|workspace| workspace.focused));
		// TODO: Show all outputs and workspaces in tabs, then have a "Current" tab as well.
		let workspace = match workspace {
			Some(w) => w,
			None => return gtk::Inhibit(false),
		};

		let layout = reingold_tilford::layout(workspace, tree::root());

		let min_x = layout.iter().fold(std::f64::INFINITY, |acc, (k, v)| {
			f64::min(
				acc,
				v.x - workspace.dimensions(*k).left - workspace.border(*k).left,
			)
		});
		let min_y = layout.iter().fold(std::f64::INFINITY, |acc, (k, v)| {
			f64::min(
				acc,
				v.y - workspace.dimensions(*k).top - workspace.border(*k).top,
			)
		});
		let max_x = layout.iter().fold(std::f64::NEG_INFINITY, |acc, (k, v)| {
			f64::max(
				acc,
				v.x + workspace.dimensions(*k).right + workspace.border(*k).right,
			)
		});
		let max_y = layout.iter().fold(std::f64::NEG_INFINITY, |acc, (k, v)| {
			f64::max(
				acc,
				v.y + workspace.dimensions(*k).bottom + workspace.border(*k).bottom,
			)
		});

		let range_x = max_x - min_x;
		let range_y = max_y - min_y;
		let width = f64::from(area.get_allocated_width());
		let height = f64::from(area.get_allocated_height());

		cairo.scale(width / range_x, height / range_y);

		cairo.set_source_rgb(0.95, 0.95, 0.95);
		cairo.paint();

		cairo.set_source_rgb(0.25, 0.25, 0.25);

		let mut stack = vec![tree::root()];

		while let Some(node) = stack.pop() {
			let reingold_tilford::Coordinate { x, y } = layout[&node];
			let reingold_tilford::Dimensions {
				top,
				bottom,
				left,
				right,
			} = workspace.dimensions(node);

			draw_node(
				x - left,
				y - top,
				left + right,
				top + bottom,
				&workspace.containers[node],
			);

			let meetpoint = y + bottom + workspace.border(node).bottom;

			for child in workspace.children(node) {
				stack.push(child);

				let reingold_tilford::Coordinate {
					x: child_x,
					y: child_y,
				} = layout[&child];

				let child_top = child_y - workspace.dimensions(child).top;

				cairo.move_to(child_x, child_top);
				cairo.line_to(child_x, meetpoint);
				cairo.line_to(x, meetpoint);
				cairo.line_to(x, y + bottom);
			}

			cairo.stroke();
		}

		gtk::Inhibit(false)
	});

	let rx_tree = tree.clone();
	rx.attach(None, move |new| {
		*rx_tree.borrow_mut() = new;

		area.queue_draw_area(
			0,
			0,
			area.get_allocated_width(),
			area.get_allocated_height(),
		);

		glib::Continue(true)
	});

	window.show_all();
}

fn main() {
	let matches = clap::app_from_crate!()
		.arg(
			clap::Arg::with_name("no-keybindings")
				.short("k")
				.long("no-keybindings")
				.help("Prevent the keybindings window from opening."),
		)
		.arg(
			clap::Arg::with_name("no-tree")
				.short("t")
				.long("no-tree")
				.help("Prevent the tree window from opening."),
		)
		.about("Visualise keybindings and container tree of current i3/sway instance.")
		.help_message("Print help information.")
		.version_message("Print version information.")
		.get_matches();

	fn version_has_config(version: &i3ipc::reply::Version) -> bool {
		version.major > 4 || (version.major == 4 && version.minor >= 14)
	}

	let mut conn = i3ipc::I3Connection::connect().unwrap();
	let version = conn.get_version().unwrap();
	let bindings = if version_has_config(&version) {
		config::read_bindings(conn.get_config().unwrap().config.as_bytes()).unwrap()
	} else {
		config::load_bindings(&version.loaded_config_file_name).unwrap()
	};

	let application =
		gtk::Application::new(Some("xyz.seamsay.i3_vis"), Default::default()).unwrap();

	application.connect_activate(move |application| {
		if !matches.is_present("no-keybindings") {
			keybindings(application, &bindings);
		}

		if !matches.is_present("no-tree") {
			tree(application);
		}
	});

	application.run(&[]);
}
